package main

import (
	"os"
	"strings"
)

func handleCommand(bottomText *string, command string, path string, selectedFile string) {
	if strings.HasPrefix(command, "touch ") {
		_, err := os.Create(path + "/" + command[6:])
		if err != nil {
			*bottomText = "Could not create " + err.Error()[5:]
		} else {
			*bottomText = "Created file '" + command[6:] + "'"
		}
	} else if strings.HasPrefix(command, "rm") {
		err := os.Remove(path + "/" + selectedFile)
		if err != nil {
			*bottomText = "Could not " + err.Error()
		} else {
			*bottomText = "Deleted file '" + selectedFile + "'"
		}
	}
}