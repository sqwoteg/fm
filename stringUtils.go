package main

import "strconv"

func trimSlash(s string) string {
	sz := len(s)
	if sz > 0 && s[sz-1] == '/' {
		return s[:sz-1]
	}
	return s
}
func afterLastSlash(s string) string {
	sz := len(s)
	if sz > 0 {
		for i := sz - 1; i >= 0; i-- {
			if s[i] == '/' {
				return s[i:]
			}
		}
	}
	return "/"
}
func trimLastPathPoint(s string) string {
	sz := len(s)
	if sz > 1 {
		for i := sz - 1; i > 1; i-- {
			if s[i] == '/' {
				return s[:i]
			}
		}
	}
	return "/"
}
func IsLetter(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
			return false
		}
	}
	return true
}
func IsNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}