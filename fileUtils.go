package main

import (
	"bufio"
	"github.com/gabriel-vasile/mimetype"
	ui "github.com/gizak/termui/v3"
	"io"
	"io/ioutil"
	"log"
	"os"
)

func hasFilesInside(path string) bool {
	file, err := os.Lstat(path)
	if err != nil {
		return false
	}
	if file.IsDir() {
		file, err := os.Open(path)
		if err != nil {
			return false
		}
		defer file.Close()
		_, err = file.Readdirnames(1)
		if err == io.EOF {
			return false
		}
		return true
	}
	if file.Mode().String()[0] == 'L' {
		linkPath, err := os.Readlink(path)
		if err != nil || len(linkPath) < 1 {
			return false
		}
		if linkPath[0] != '/' {
			linkPath = trimLastPathPoint(path) + "/" + linkPath
		}
		return hasFilesInside(linkPath)
	}
	return false
}
func previewFileOrDir(path string) []string {
	file, err := os.Lstat(path)
	if err != nil {
		return []string{"Something went wrong.."}
	}

	var array []string
	if file.IsDir() {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			return []string{"Permission denied."}
		}

		if len(files) > 0 {
			for _, f := range files {
				array = append(array, f.Name())
			}
		} else {
			return []string{"Empty."}
		}
		return array
	} else {
		if file.Mode().String()[0] == 'L' {
			linkPath, err := os.Readlink(path)
			if err != nil || len(linkPath) < 1 {
				var e string
				if e = "Empty link"; err != nil {e = err.Error()}
				return []string{"Something went wrong..", e}
			}
			if linkPath[0] != '/' {
				linkPath = trimLastPathPoint(path) + "/" + linkPath
			}
			return previewFileOrDir(linkPath)
		} else if file.Mode().IsRegular() {
			return previewFile(path)
		} else {
			return []string{"Could not load preview."}
		}
	}
}
func previewFile(path string) []string {
	mime, err := mimetype.DetectFile(path)
	if err != nil || !mime.Is("text/plain") {
		return []string{"Mime type is not supported."}
	}
	file, err := os.Open(path)
	if err != nil {
		return []string{"Something went wrong.."}
	}
	defer file.Close()

	_, termHeight := ui.TerminalDimensions()
	i := 0
	var fileContent []string

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		if i >= termHeight {break}
		i++
		fileContent = append(fileContent, fileScanner.Text())
	}

	if err := fileScanner.Err(); err != nil {
		log.Fatal(err)
	}
	return fileContent
}