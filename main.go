package main

import (
	"github.com/gabriel-vasile/mimetype"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
)

func main() {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	windowSizeType := 3

	termWidth, termHeight := ui.TerminalDimensions()

	currentPath, err := os.Getwd()
	if err != nil {
		currentPath = "/"
	}
	currentPathFiles := listDirectory(currentPath)
	//maxElementsInPreview := 30
	//if terminalHeight < 30 {maxElementsInPreview = terminalHeight}

	current := widgets.NewList()
	current.BorderTop = true
	current.BorderLeft = false
	current.BorderRight = false
	current.BorderBottom = false
	current.Title = " Current directory "
	//current.TitleStyle = ui.NewStyle(ui.ColorYellow, ui.ColorBlack, ui.ModifierUnderline)
	current.Rows = directoryListToColoredNamesArray(currentPathFiles, currentPath)

	current.TextStyle = ui.NewStyle(ui.ColorWhite)
	current.SelectedRowStyle = ui.NewStyle(ui.ColorBlack, ui.ColorWhite)
	current.PaddingTop = 1

	spacer := ui.NewBlock()
	spacer.Border = false

	previewFolder := widgets.NewList()
	previewFolder.BorderTop = true
	previewFolder.BorderLeft = false
	previewFolder.BorderRight = false
	previewFolder.BorderBottom = false
	previewFolder.Title = " Preview "
	previewFolder.Rows = previewFileOrDir(currentPath + "/" + currentPathFiles[current.SelectedRow].Name())
	previewFolder.TextStyle = ui.NewStyle(ui.ColorWhite)
	previewFolder.PaddingTop = 1

	grid := ui.NewGrid()
	grid.SetRect(0, 0, termWidth, termHeight-1)

	if windowSizeType == 3 {
		grid.Set(
			ui.NewRow(1.0,
				ui.NewCol(0.48, current),
				ui.NewCol(1.0/25, spacer),
				ui.NewCol(0.48, previewFolder),
			),
		)
	} else {
		grid.Set(
			ui.NewRow(1.0,
				ui.NewCol(1.0, current),
			),
		)
	}


	currentPathPar := widgets.NewParagraph()
	currentPathPar.Text = trimSlash(currentPath) + "/" + currentPathFiles[current.SelectedRow].Name()
	currentPathPar.TextStyle = ui.NewStyle(ui.ColorYellow)
	currentPathPar.Border = false
	currentPathPar.SetRect(0, termHeight - 1, termWidth, termHeight)

	ui.Render(grid)
	ui.Render(currentPathPar)

	commandInput := false
	commandInputText := ""

	uiEvents := ui.PollEvents()
	for {
		e := <-uiEvents
		switch e.ID {
		case "<C-c>":
			return
		case "<Down>":
			current.ScrollDown()
			previewFolder.Rows = previewFileOrDir(currentPath + "/" + currentPathFiles[current.SelectedRow].Name())
		case "<Up>":
			current.ScrollUp()
			previewFolder.Rows = previewFileOrDir(currentPath + "/" + currentPathFiles[current.SelectedRow].Name())
		case "<Left>":
			prevDirectory := afterLastSlash(trimSlash(currentPath))[1:]
			currentPath = trimLastPathPoint(trimSlash(currentPath))
			currentPathFiles = listDirectory(currentPath)
			current.Rows = directoryListToColoredNamesArray(currentPathFiles, currentPath)
			current.SelectedRow = sort.SearchStrings(directoryListToNamesArray(currentPathFiles), prevDirectory)
			previewFolder.Rows = previewFileOrDir(currentPath + "/" + currentPathFiles[current.SelectedRow].Name())
		case "<Right>":
			if !hasFilesInside(trimSlash(currentPath) + "/" + currentPathFiles[current.SelectedRow].Name()) {
				break
			}
			currentPath = trimSlash(currentPath) + "/" + currentPathFiles[current.SelectedRow].Name()
			currentPathFiles = listDirectory(currentPath)
			current.Rows = directoryListToColoredNamesArray(currentPathFiles, currentPath)
			if len(currentPathFiles) > 0 {
				current.SelectedRow = 0
				previewFolder.Rows = previewFileOrDir(currentPath + "/" + currentPathFiles[current.SelectedRow].Name())
			} else {
				previewFolder.Rows = []string{}
			}

		//case "<C-d>":
		//	current.ScrollHalfPageDown()
		//case "<C-u>":
		//	current.ScrollHalfPageUp()
		//case "<C-f>":
		//	current.ScrollPageDown()
		//case "<C-b>":
		//	current.ScrollPageUp()
		//case "<Home>":
		//	current.ScrollTop()
		//case "G", "<End>":
		//	current.ScrollBottom()
		case "<Resize>":
			payload := e.Payload.(ui.Resize)
			grid.SetRect(0, 0, payload.Width, payload.Height-1)
			ui.Render(grid)
		}

		currentPathPar.Text = trimSlash(currentPath) + "/" + currentPathFiles[current.SelectedRow].Name()


		if e.ID == ":" {
			if commandInput {
				commandInputText += e.ID
			} else {
				commandInput = true
			}
		}

		if commandInput {
			if e.ID == "<Escape>" {
				commandInput = false
				commandInputText = ""
			} else if e.ID == "<Enter>" {
				commandInput = false
				handleCommand(&currentPathPar.Text, commandInputText, currentPath, currentPathFiles[current.SelectedRow].Name())
				commandInputText = ""
				currentPathFiles = listDirectory(currentPath)
				current.Rows = directoryListToColoredNamesArray(currentPathFiles, currentPath)
				if len(currentPathFiles) > 0 {
					current.SelectedRow = 0
				} else {
					previewFolder.Rows = []string{}
				}
			} else if e.ID == "<Backspace>" {
				length := len(commandInputText)
				if length > 1 {
					commandInputText = commandInputText[:length - 1]
					currentPathPar.Text = ":" + commandInputText
				} else {
					commandInput = false
					commandInputText = ""
					currentPathPar.Text = trimSlash(currentPath) + "/" + current.Rows[current.SelectedRow]
				}
			} else {
				if IsLetter(e.ID) || IsNumeric(e.ID) {
					commandInputText += e.ID
				} else if e.ID == "<Space>" {
					commandInputText += " "
				}
				currentPathPar.Text = ":" + commandInputText
			}
		}

		ui.Render(current)
		ui.Render(previewFolder)
		ui.Render(currentPathPar)
	}
}

func listDirectory(path string) []os.FileInfo {
	file, err := os.Lstat(path)
	if err != nil {
		return []os.FileInfo{}
	}
	if file.IsDir() {
		files, err := ioutil.ReadDir(path)
		if err != nil {
			return []os.FileInfo{}
		}
		return files
	} else if file.Mode().String()[0] == 'L' {
		linkPath, err := os.Readlink(path)
		if err != nil || len(linkPath) < 1 {
			return []os.FileInfo{}
		}
		if linkPath[0] != '/' {
			linkPath = trimLastPathPoint(path) + "/" + linkPath
		}
		files, err := ioutil.ReadDir(linkPath)
		if err != nil {
			return []os.FileInfo{}
		}
		return files
	}
	return []os.FileInfo{}
}
func directoryListToColoredNamesArray(files []os.FileInfo, path string) []string {
	var array []string
	for _, f := range files {
		if f.IsDir() {
			array = append(array, "⤷ [" + f.Name() + "](mod:bold)  ")
		} else if f.Mode().String()[0] == 'L' {
			array = append(array, "[↣](fg:yellow) " + f.Name() + "  ")
		} else {
			mime, err := mimetype.DetectFile(trimSlash(path) + "/" + f.Name())
			if err != nil {
				array = append(array, "  [" + f.Name() + "](fg:red)  " + err.Error())
			} else if mime.Is("text/plain") {
				array = append(array, "  [" + f.Name() + "](fg:green)  ")
			} else if strings.HasPrefix(mime.String(), "image/") {
				array = append(array, "  [" + f.Name() + "](fg:yellow)  ")
			} else if strings.HasPrefix(mime.String(), "video/") {
				array = append(array, "  [" + f.Name() + "](fg:yellow)  ")
			} else if strings.HasPrefix(mime.String(), "audio/") {
				array = append(array, "  [" + f.Name() + "](fg:yellow)  ")
			} else {
				array = append(array, "  " + f.Name() + "  ")
			}

		}
		//array = append(array, f.Name())
	}
	return array
}
func directoryListToNamesArray(files []os.FileInfo) []string {
	var array []string
	for _, f := range files {
		array = append(array, f.Name())
	}
	return array
}
